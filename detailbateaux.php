<?php 

$titre = "Detail du bateaux";


require("bdd/bddconfig.php");

try {

    $bateauID = $_GET['bateaux'];


    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recupBateau = $objBdd->query("SELECT * FROM `skipper` WHERE skipper.idBateau = $bateauID");

    $recupImg = $objBdd->query("SELECT bateau.photo, bateau.nomBateau FROM `bateau` WHERE bateau.idBateau = $bateauID");


    $bateau = $recupImg->fetch();
    $pilote = $recupBateau->fetch();

    


} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}


?>

<section>
    <article>

        <div class="img-bateau">
            <img src="images/bateaux/<?php echo $bateau[0]  ?>" alt="Photo du bateau">
            <p><?php echo $bateau[1] ?></p>
        </div>

        <div>
            <img src="images/skippers/<?php echo $pilote['photo']  ?>" alt="Photo du Pilote">
            <p><?php echo $pilote["nomSkipper"] ?></p>
        </div>

    </article>
</section>
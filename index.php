<?php 

session_start();

if(!isset($_SESSION['logged_in']["id"])){
    $_SESSION["connexion"] = 0;
}else{
    $_SESSION["connexion"] = $_SESSION['logged_in']["id"];
}



?>



<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Transat Jacques Vabre 2007</title>
        <link rel="stylesheet" href="css/transat.css" />
    </head>
    <body>
        <div id="conteneur">
            <header>
                <div class='login'>
                    <form method="POST" action="admin/login_action.php">
                        <fieldset>
                            <legend>Connectez-vous</legend>
                            <input type="text" name="login" value="" placeholder="Login" required>
                            <input type="text" name="password" value="" placeholder="Password" required>
                            <input type="submit" value="Valider">
                        </fieldset>
                    </form>   
                </div>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php?page=accueil" class="navitem">Accueil</a></li>
                    <li><a href="index.php?page=classements" class="navitem">Classements</a></li>

                    <?php if($_SESSION["connexion"] != 0){ ?>
                    
                        <li><a href="index.php?page=ajoutclasse" class="navitemprivate">Ajout Classe</a></li>
                        <li><a href="index.php?page=logout" class="navitemprivate">Deconnexion</a></li>

                    <?php } ?>
                </ul>
            </nav>
            
            <?php 
                if (!isset($_GET['page']) || !file_exists($_GET['page'] . '.php')) {
                    require_once('accueil.php');
                } else {
                    require_once($_GET['page'] . ".php");
                }
            ?>

            <footer>
                <p>Copyright Moi - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>
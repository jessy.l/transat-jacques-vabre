<?php 

session_start();

if($_SESSION["connexion"] == 0){
    header("Location: ../index.php");
}else{


    $classe = htmlspecialchars($_POST["classe"]);
    $engagee = htmlspecialchars($_POST["engagee"]);
    $type = htmlspecialchars($_POST["type"]);
    $taille = htmlspecialchars($_POST["taille"]);


    require('../bdd/bddconfig.php');

    try {

        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $PDOinsert = $objBdd->prepare("INSERT INTO `classebateau` (nomClasse, engagee, typeCoque, tailleCoque) VALUES (:classe,:engagee,:type,:taille  )");
            $PDOinsert->bindParam(':classe', $classe, PDO::PARAM_STR);
            $PDOinsert->bindParam(':engagee', $engagee, PDO::PARAM_STR);
            $PDOinsert->bindParam(':type', $type, PDO::PARAM_STR);
            $PDOinsert->bindParam(':taille', $taille, PDO::PARAM_STR);

            $PDOinsert->execute();


            header("Location: ../index.php");

    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }

    
}

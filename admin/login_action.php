<?php


session_start();

require('../bdd/bddconfig.php');

//Tester si les variables POST existent
$paramOK = false;
if (isset($_POST["login"])) {
    $login = strtolower(htmlspecialchars($_POST["login"]));
    if (isset($_POST["password"])) {
        $password = htmlspecialchars($_POST["password"]);
        $paramOK = true;
    }
}

//si login et password sont bien reçus
if ($paramOK == true) {

    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //vérifier si le login passord est correct (base de données) : voir après
    $PDOlistlogins = $objBdd->prepare("SELECT * FROM login WHERE login = :login ");
    $PDOlistlogins->bindParam(':login', $login, PDO::PARAM_STR);
    $PDOlistlogins->execute();
    //S'il y a un résultat à la requête 
    $row_userweb = $PDOlistlogins->fetch();
    if ($row_userweb != false) {
        //il existe un login identique dans la base

        // vérif du password :
        if (password_verify($password, $row_userweb['password'])) {
            //authentification réussie
            //création de la variable de session : 


            $session_data = array(
                'id' => $row_userweb['idUser'],
                'login' => $row_userweb['login'],
                'nom' => $row_userweb['nom'],
                'fonction' => $row_userweb['fonction']
            );
            //régénérer le session id

            

            session_regenerate_id();
            //enregistrer les données dans une variable de session
            $_SESSION['logged_in'] = $session_data;
            $_SESSION['logged_in']["id"] = $row_userweb['idUser'];
            $PDOlistlogins->closeCursor();
        } else {
            //Mauvais password
            session_destroy();
            die('Authentification incorrecte');
        }
    } else {
        //Mauvais login
        session_destroy();
        die('Authentification incorrecte');
    }
} else {
    die('Vous devez fournir un login et un mot de passe');
}


header("Location: ../index.php");
<?php 


$login = htmlspecialchars($_POST["login"]);
$password_clair =  htmlspecialchars(strval($_POST["password"]));
$confirm_password = htmlspecialchars(strval($_POST["cpassword"]));

if($password_clair == $confirm_password){

    $hash_password = password_hash($password_clair, PASSWORD_BCRYPT);

    //Connexion à la base et insertion du nouvel utilisateur

    require '../bdd/bddconfig.php';
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $PDOinsertuserweb = $objBdd->prepare("INSERT INTO login (login, password) VALUES ( :login, :password)");
            $PDOinsertuserweb->bindParam(':login', $login, PDO::PARAM_STR);
            $PDOinsertuserweb->bindParam(':password', $hash_password, PDO::PARAM_STR);


            $PDOinsertuserweb->execute();
            //récupérer la valeur de l'ID du nouveau bassin créé
            echo $lastId = $objBdd->lastInsertId();


    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }

}

header("Location: ../index.php");
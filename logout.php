<?php


session_start();

//détruire la session courante
session_destroy();

/* Redirige vers la page d'accueil */

// Redirige vers la page d'accueil (ou login.php) si pas authentifié

header("Location: index.php");

<?php 


$titre = "Classement";


require("bdd/bddconfig.php");

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recupClass = $objBdd->query("SELECT * FROM `classebateau`");


} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}

?>


<section>

<ul>

    <?php 
    
    while ($classe = $recupClass->fetch()) {
    
        
    ?>
    
        <li> <?php echo $classe["typeCoque"] ?> <a href="index.php?page=listebateaux&classe=<?php echo $classe['idClasse'] ?>"> <?php echo $classe["nomClasse"] ?></a></li>

    <?php
    }
    $recupClass->closeCursor();?>

</ul>

</section>
<?php 
$titre = "liste des bateau";

require("bdd/bddconfig.php");

try {

    $classe = $_GET['classe'];


    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recupBateau = $objBdd->query("SELECT * FROM `bateau`, `classebateau` WHERE bateau.idClasse = $classe GROUP BY bateau.nomBateau ORDER BY bateau.classementFinal ASC ");


} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}

?>


<section>

<ul>

    <?php 
    
    while ($bateau = $recupBateau->fetch()) {
    
        if($bateau["classementFinal"] == 9999){
            $bateauClassement = "AB";
        }else{
            $bateauClassement = $bateau["classementFinal"];
        }
        
    ?>
    
        <li> <?php echo $bateauClassement ?> <a href="index.php?page=detailbateaux&bateaux=<?php echo $bateau['idBateau'] ?>"> <?php echo $bateau["nomBateau"] ?></a></li>

    <?php
    }
    $recupBateau->closeCursor();?>

</ul>

</section>